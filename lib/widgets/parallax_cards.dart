//import 'package:vircio/onboarding/gradient/utils/page_transformer.dart';
//import 'package:vircio/onboarding/gradient/utils/textstyles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:meta/meta.dart';
import 'package:vircio_new/models/colors.dart';
import 'package:vircio_new/models/gradient.dart';
import 'package:vircio_new/models/gradient_text.dart';
import 'package:vircio_new/utils/textstyles.dart';
import 'package:vircio_new/widgets/page_transformer.dart';

class ParallaxCardItem {
  ParallaxCardItem({
    this.title,
    this.body,
    this.imagePath,
    this.url,
    this.risk,
    this.riskText,
    this.selected = false,
  });

  final String title;
  final String body;
  final String imagePath;
  final String url;
  final String risk;
  final String riskText;
  bool selected;
}

//these cards are modified from https://github.com/roughike/page-transformer

class ParallaxCards extends StatefulWidget {
  ParallaxCards({
    @required this.item,
    @required this.pageVisibility,
    this.index,
    this.length,
  });

  final ParallaxCardItem item;
  final PageVisibility pageVisibility;
  final int index;
  final int length;

  @override
  _ParallaxCardsState createState() => _ParallaxCardsState();
}

class _ParallaxCardsState extends State<ParallaxCards> {
  Widget _applyTextEffects({
    @required double translationFactor,
    @required Widget child,
  }) {
    final double xTranslation =
        widget.pageVisibility.pagePosition * translationFactor;

    return Opacity(
      opacity: widget.pageVisibility.visibleFraction,
      child: Transform(
        alignment: FractionalOffset.topLeft,
        transform: Matrix4.translationValues(
          xTranslation,
          0.0,
          0.0,
        ),
        child: child,
      ),
    );
  }

  _buildTextContainer(BuildContext context) {
    double c_width = MediaQuery.of(context).size.width * 0.2;
    var riskText = _applyTextEffects(
      translationFactor: 200.0,
      child: Container(
        width: c_width,
        child: Text(
          widget.item.riskText,
          style: TextStyle(
              fontFamily: 'RobotoMono',
              fontWeight: FontWeight.w500,
              fontSize: 12.0,
              color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ),
    );
    var categoryText = _applyTextEffects(
      translationFactor: 300.0,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: GradientText(
          widget.item.body,
          shaderRect: Rect.fromLTWH(0.0, 0.0, 50.0, 50.0),
          style: MyTextStyles.gradientCardBodyStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
    var risk = _applyTextEffects(
      translationFactor: 200.0,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: <Widget>[
            ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: 100),
              child: Image.asset(
                widget.item.risk,
              ),
            ),
            riskText
          ],
        ),
      ),
    );

    var titleText = _applyTextEffects(
      translationFactor: 200.0,
      child: Padding(
        padding: EdgeInsets.all(5.0),
        child: GradientText(
          widget.item.title,
          shaderRect: Rect.fromLTWH(0.0, 0.0, 50.0, 50.0),
          gradient: Gradients.haze,
          style: MyTextStyles.gradientCardTitleStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );

    return Positioned(
      bottom: 10.0,
      left: 10.0,
      right: 10.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          risk,
          titleText,
          categoryText,
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var image = Image.asset(
      widget.item.imagePath,
      fit: BoxFit.cover,
      alignment: FractionalOffset(
        0.5 + (widget.pageVisibility.pagePosition / 3),
        0.5,
      ),
    );

    var imageOverlayGradient = DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(36.0),
        border: Border.all(
            color: widget.item.selected
                ? AppColors.buttonColor
                : Color(0xffd4dadb),
            width: 4.0),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.transparent,
            Colors.black,
          ],
        ),
      ),
    );

    return GestureDetector(
      //onTap: () => Navigator.pushNamed(context, item.url),
      onTap: () {
        setState(() {
          for (int i = 0; i < widget.length; i++) {
            if (i == widget.index) {
              widget.item.selected = true;
            } else {
              widget.item.selected = false;
            }
          }
        });
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 22.0,
          horizontal: 8.0,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(36.0),
          child: Material(
            child: Stack(
              fit: StackFit.expand,
              children: [
                image,
                imageOverlayGradient,
                _buildTextContainer(context),
                widget.item.selected
                    ? Positioned(
                        top: 15.0,
                        left: 15.0,
                        child: CircleAvatar(
                          radius: 18.0,
                          backgroundColor: Colors.white,
                          child: Container(
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: AppColors.buttonColor,
                            ),
                            child: Icon(Icons.check,
                                color: Colors.white, size: 24.0),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
