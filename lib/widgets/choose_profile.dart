// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:vircio_new/widgets/page_transformer.dart';
// import 'package:vircio_new/widgets/parallax_cards.dart';
// //import 'package:vircio/widgets/page_transformer.dart';
// //import 'package:vircio/widgets/parallax_cards.dart';

// class ChooseProfile extends StatefulWidget {
//   final int selectedIndex;
//   ChooseProfile(this.selectedIndex);

//   @override
//   _ChooseProfileState createState() => _ChooseProfileState();
// }

// final parallaxCardItemsList = <ParallaxCardItem>[
//   ParallaxCardItem(
//     title: 'Aggressive Investor',
//     value: 'Aggressive',
//     body: 'For the young and restless',
//     imagePath: 'assets/images/chooseProfile/young.webp',
//     url: "/homepage",
//     risk: 'assets/images/chooseProfile/high-risk.png',
//     riskText: "High Risk",
//     titleKey: "aggressive_investor",
//     typeKey: "aggressive_type",
//     riskKey: "aggressive_risk"
//   ),
//   ParallaxCardItem(
//     title: 'Moderately Aggressive Investor',
//     value: 'Moderately Aggressive',
//     body: 'Cautiously optimistic on markets',
//     imagePath: 'assets/images/chooseProfile/couple.png',
//     url: "/homepage",
//     risk: 'assets/images/chooseProfile/moderately-high-risk.png',
//     riskText: "Moderately High Risk",
//     titleKey: "moderately_aggressive",
//     typeKey: "mod_aggr_type",
//     riskKey: "mod_aggr_risk"
//   ),
//   ParallaxCardItem(
//     title: 'Balanced Investor',
//     value: 'Balanced',
//     body: 'Get the best of both worlds',
//     imagePath: 'assets/images/chooseProfile/balanced.jpg',
//     url: "/homepage",
//     risk: 'assets/images/chooseProfile/moderate-risk.png',
//     riskText: "Moderate Risk",
//     titleKey: "balanced_investor",
//     typeKey: "balanced_investor_type",
//     riskKey: "balanced_investor_risk"
//   ),
//   ParallaxCardItem(
//     title: 'Moderately Conservative Investor',
//     value: 'Moderately Conservative',
//     body: 'When safety is paramount',
//     imagePath: 'assets/images/chooseProfile/retirement1.jpg',
//     url: "/homepage",
//     risk: 'assets/images/chooseProfile/moderately-low-risk.png',
//     riskText: "Moderately Low Risk",
//     titleKey: "moderately_conservative",
//     typeKey: "mod_conserv_type",
//     riskKey: "mod_conserv_risk"
//   ),
//   ParallaxCardItem(
//     title: 'Conservative Investor',
//     value: 'Conservative',
//     body: 'Regular income in the golden years',
//     imagePath: 'assets/images/chooseProfile/retirement2.png',
//     url: "/homepage",
//     risk: 'assets/images/chooseProfile/low-risk.png',
//     riskText: "Low Risk",
//     titleKey: "conservative_investor",
//     typeKey: "conserv_investor_type",
//     riskKey: "conserv_investor_risk"
//   ),
//   ParallaxCardItem(
//     title: 'Confused ?',
//     body: 'Create a custom profile',
//     imagePath: 'assets/images/chooseProfile/confused.png',
//     url: "/queone",
//     risk: 'assets/images/chooseProfile/question-mark.png',
//     riskText: "Take a questionnaire",
//     titleKey: "confused_profile",
//     typeKey: "confused_type",
//     riskKey: "create_profile"
//   )
// ];

// class _ChooseProfileState extends State<ChooseProfile> {

//   @override
//   Widget build(BuildContext context) {
//     return
//         Padding(
//           padding: EdgeInsets.only(top:25, bottom: 30.0),
//           child: SizedBox.fromSize(
//             size: Size.fromHeight(500.0),
//             child: PageTransformer(
//               pageViewBuilder: (context, visibilityResolver) {
//                 return PageView.builder(
//                   controller: PageController(viewportFraction: 0.85),
//                   itemCount: parallaxCardItemsList.length,
//                   itemBuilder: (context, index) {
//                     final item = parallaxCardItemsList[index];
//                     final pageVisibility =
//                         visibilityResolver.resolvePageVisibility(index);
//                     return ParallaxCards(
//                       item: item,
//                       pageVisibility: pageVisibility,
//                     );
//                   },
//                 );
//               },
//             ),
//           ),
//     );
//   }
// }
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vircio_new/widgets/page_transformer.dart';
import 'package:vircio_new/widgets/parallax_cards.dart';
import 'package:vircio_new/widgets/submit_button.dart';

class ChooseProfile extends StatefulWidget {
  //final int selectedIndex;
  //ChooseProfile(this.selectedIndex);

  @override
  _ChooseProfileState createState() => _ChooseProfileState();
}

final parallaxCardItemsList = <ParallaxCardItem>[
  ParallaxCardItem(
    title: 'Aggressive Investor',
    body: 'For the young and restless',
    imagePath: 'assets/images/chooseProfile/young.webp',
    url: "/enterphone",
    risk: 'assets/images/chooseProfile/high-risk.png',
    riskText: "High Risk",
  ),
  ParallaxCardItem(
    title: 'Moderately Aggressive Investor',
    body: 'Cautiously optimistic on markets',
    imagePath: 'assets/images/chooseProfile/couple.png',
    url: "/enterphone",
    risk: 'assets/images/chooseProfile/moderately-high-risk.png',
    riskText: "Moderately High Risk",
  ),
  ParallaxCardItem(
    title: 'Balanced Investor',
    body: 'Get the best of both worlds',
    imagePath: 'assets/images/chooseProfile/balanced.jpg',
    url: "/enterphone",
    risk: 'assets/images/chooseProfile/moderate-risk.png',
    riskText: "Moderate Risk",
  ),
  ParallaxCardItem(
    title: 'Moderately Conservative Investor',
    body: 'When safety is paramount',
    imagePath: 'assets/images/chooseProfile/retirement1.jpg',
    url: "/enterphone",
    risk: 'assets/images/chooseProfile/moderately-low-risk.png',
    riskText: "Moderately Low Risk",
  ),
  ParallaxCardItem(
    title: 'Conservative Investor',
    body: 'Regular income in the golden years',
    imagePath: 'assets/images/chooseProfile/retirement2.png',
    url: "/enterphone",
    risk: 'assets/images/chooseProfile/low-risk.png',
    riskText: "Low Risk",
  ),
  ParallaxCardItem(
    title: 'Confused ?',
    body: "Take a questionnaire",
    imagePath: 'assets/images/chooseProfile/confused.png',
    url: "/queone",
    risk: 'assets/images/chooseProfile/question-mark.png',
    riskText: 'Create a custom profile',
  ),
];

class _ChooseProfileState extends State<ChooseProfile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
       
        Padding(
          padding: EdgeInsets.only(top: 10, bottom: 0.0),
          child: SizedBox.fromSize(
            size: Size.fromHeight(480),
            child: PageTransformer(
              pageViewBuilder: (context, visibilityResolver) {
                return PageView.builder(
                  controller: PageController(viewportFraction: 0.85),
                  itemCount: parallaxCardItemsList.length,
                  itemBuilder: (context, index) {
                    final item = parallaxCardItemsList[index];
                    final pageVisibility =
                        visibilityResolver.resolvePageVisibility(index);
                    return ParallaxCards(
                      item: item,
                      pageVisibility: pageVisibility,
                      index:index,
                      length: parallaxCardItemsList.length,
                    );
                  },
                );
              },
            ),
          ),
        ),
        SubmitButton()
      ],
    );
  }
}
