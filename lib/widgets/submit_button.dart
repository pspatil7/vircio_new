import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  final Function onpressed;
  SubmitButton({this.onpressed});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: 179,
      height: 46,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(22),
      ),
      color: Color(0xff13a89e),
      child: Text(
        "Submit",
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 18,
          fontWeight: FontWeight.w800,
          color: Colors.white,
        ),
      ),
      onPressed: onpressed,
    );
  }
}
