import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85.0,
      width: 85.0,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/vircio_logo.png"),
        ),
      ),
    );
  }
}
