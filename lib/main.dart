import 'package:flutter/material.dart';
import 'package:vircio_new/Screens/persona_selection.dart';
import 'Screens/personal_info.dart';
import 'Screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
      ),
      home: SplashScreen(),
      routes: {
        '/personal_info': (context) => PersonalInfo(),
        '/persona':(context)=> PersonaSelection(),
      },
    );
  }
}

