import 'dart:async';

import 'package:flutter/material.dart';

import 'Welcome_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
   startTimeout() {
    return  Timer(Duration(seconds: 3), changeScreen);
  }
    changeScreen() async{
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context){
          return WelcomeScreen();
        },
      ),
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTimeout();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                "assets/images/splash1.png",
              ),
              fit: BoxFit.cover,
            ),
        ),
      ),
    );
  }
}
