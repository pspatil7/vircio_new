import 'package:flutter/material.dart';
import 'package:vircio_new/models/colors.dart';
import 'package:vircio_new/widgets/logo.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.grey,
      body: Padding(
        padding: EdgeInsets.only(top: 70, bottom: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           Logo(),
            Column(
              children: <Widget>[
                Text(
                  "Welcome",
                  style: TextStyle(
                    letterSpacing: 0.5,
                    fontFamily: 'Montserrat',
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    color: AppColors.primaryText,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Discover a new ways of investment",
                  style: TextStyle(
                    letterSpacing: 0.5,
                    fontFamily: 'Montserrat',
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: AppColors.primaryText,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 35.0, vertical: 30.0),
              child: FlatButton(
                padding: EdgeInsets.all(4.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    50.0,
                  ),
                  side: BorderSide(color: Color(0xff44285f4), width: 1.2),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, "/personal_info");
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 25.0,
                        width: 25.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/images/google.png"),
                          ),
                        ),
                      ),
                      Text(
                        "Signup With Google",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Color(0xff5266fe8),
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 0.0),
              child: Container(
                height: 250.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/welcome_vircio.png"),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
