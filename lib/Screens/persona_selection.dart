import 'package:flutter/material.dart';
import 'package:vircio_new/models/colors.dart';
import 'package:vircio_new/widgets/choose_profile.dart';
import 'package:vircio_new/widgets/submit_button.dart';
//import 'package:vircio_new/widgets/persona_selection.dart';
//import 'package:vircio/widgets/PersonaSelection.dart';

//import 'package:easy_localization/easy_localization.dart';

class PersonaSelection extends StatefulWidget {
  @override
  _PersonaSelectionState createState() => _PersonaSelectionState();
}

class _PersonaSelectionState extends State<PersonaSelection> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(height * 0.13),
        child: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: AppColors.primaryText, //change your color here
          ),

          elevation: 0,
          //centerTitle: true,

          bottom: PreferredSize(
            preferredSize: null,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Choose Your Persona",
                  style: TextStyle(
                    letterSpacing: 0.5,
                    fontFamily: 'Montserrat',
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                    color: AppColors.primaryText,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "This will help us to understand your need",
                  style: TextStyle(
                    letterSpacing: 0.3,
                    fontFamily: 'Montserrat',
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: AppColors.primaryText,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 10.0),
              child: ChooseProfile(),
            ),
          ),
          SubmitButton(onpressed: () {}),
        ],
      ),
    );
  }
}
