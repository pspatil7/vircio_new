import 'package:flutter/material.dart';
import 'package:vircio_new/models/colors.dart';
import 'package:vircio_new/widgets/logo.dart';
import 'package:vircio_new/widgets/submit_button.dart';

class PersonalInfo extends StatefulWidget {
  @override
  _PersonalInfoState createState() => _PersonalInfoState();
}

class _PersonalInfoState extends State<PersonalInfo> {
  List<Residence> _residence = [
    Residence(buttonText: "Resident Indian "),
    Residence(buttonText: "Non-Resident Indian"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 40.0, left: 25.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: CircleAvatar(
                  radius: 18.0,
                  backgroundColor: AppColors.borderColor,
                  child: IconButton(
                    icon: Center(
                      child: Icon(
                        Icons.arrow_back,
                        size: 20.0,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: AppColors.primaryText,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Logo(),
            SizedBox(height: 45.0),
            Text(
              "Personal Info",
              style: TextStyle(
                letterSpacing: 0.5,
                fontFamily: 'Montserrat',
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: AppColors.primaryText,
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              "your input is mandatory!",
              style: TextStyle(
                letterSpacing: 0.2,
                fontFamily: 'Montserrat',
                fontSize: 15,
                fontWeight: FontWeight.w600,
                //  color: AppColors.primaryText,
              ),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 35.0,
                left: 30,
                right: 30.0,
                bottom: 20.0,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16.0),
                  border: Border.all(color: AppColors.borderColor),
                  color: AppColors.fillColor,
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Mobile Number - Enter 10 digits",
                    hintStyle: TextStyle(
                        // letterSpacing: 0.5,
                        fontSize: 15.0,
                        fontFamily: 'Montserrat',
                        color: Colors.grey,
                        fontWeight: FontWeight.w600),
                    contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 30, right: 30.0, bottom: 20.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  border: Border.all(color: AppColors.borderColor),
                  color: AppColors.fillColor,
                ),
                child: TextField(
                  decoration: InputDecoration(
                    // border: OutlineInputBorder(
                    //     borderRadius: BorderRadius.circular(12.0),
                    //     borderSide: BorderSide(color: Colors.blueGrey[50]),
                    //     ),
                    border: InputBorder.none,
                    hintText: "PAN Number",
                    hintStyle: TextStyle(
                        // letterSpacing: 0.5,
                        fontSize: 15.0,
                        fontFamily: 'Montserrat',
                        color: Colors.grey,
                        fontWeight: FontWeight.w600),
                    contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 30,
                right: 30.0,
                bottom: 30.0,
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  border: Border.all(color: AppColors.borderColor),
                  color: AppColors.fillColor,
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Broker ID",
                    hintStyle: TextStyle(
                        // letterSpacing: 0.5,
                        fontSize: 15.0,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600,
                        color: Colors.grey),
                    contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 35.0, bottom: 10),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "I am",
                  style: TextStyle(
                      letterSpacing: 0.2,
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff990f1f42)),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 30, right: 30.0),
              child: Container(
                //  color: Colors.white,
                width: double.infinity,
                height: 45.0,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: _residence.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(color: AppColors.borderColor)),
                      color: _residence[index].changeButtonColor
                          ? Color(0xff135e73)
                          : AppColors.fillColor,
                      onPressed: () {
                        setState(() {
                          for (int i = 0; i < _residence.length; i++) {
                            if (i == index) {
                              _residence[i].changeButtonColor = true;
                            } else {
                              _residence[i].changeButtonColor = false;
                            }
                          }
                        });
                      },
                      child: Text(
                        _residence[index].buttonText,
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.2,
                          color: _residence[index].changeButtonColor
                              ? Colors.white
                              : Colors.grey,
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(width: 5.0);
                  },
                ),
              ),
            ),
            SizedBox(height: 40.0),
            SubmitButton(
              onpressed: () => Navigator.pushNamed(context, '/persona'),
            )
          ],
        ),
      ),
    );
  }
}

class Residence {
  final String buttonText;
  bool changeButtonColor;
  Residence({this.buttonText, this.changeButtonColor = false});
}
