


import 'package:flutter/material.dart';

class AppColors{
  static Color fillColor = Color(0xfff2f5fa);// Colors.blueGrey[700],
  static  Color primaryText =  Color(0xff5172e5d);
  static const Color tealGreen = Color(0xff089699);
  static const Color borderColor = Color(0xff4dde2ea);
  static const Color buttonColor = Color(0xff1895a8);
}